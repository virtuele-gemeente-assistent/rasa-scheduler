from sanic import Blueprint, Sanic, response, exceptions
from sanic.request import Request
import aiohttp
from sanic.response import HTTPResponse
from auth import protected
from abc import ABC, abstractmethod
import datetime# import redis
import logging
import asyncio
import redis
import json
import time
import os

logger = logging.getLogger(__name__)
RASA_URL = os.getenv("RASA_URL")
REDIS_URL = os.getenv("REDIS_URL")
REDIS_PREFIX = os.getenv("REDIX_PREFIX","rasa-schedule/")

class Reminder():
    def __init__(self, reminder_manager = None, sender_id:str = None, name:str = None, intent:str = None, entities:dict = None, output_channel:str = None, schedule = None, kill_on_user_message = False):
        if isinstance(schedule,float):
            self.schedule = datetime.datetime.fromtimestamp(schedule)
        elif isinstance(schedule,str):
            self.schedule = datetime.datetime.fromisoformat(schedule)
        else:
            self.schedule = schedule

        self.sender_id = sender_id #
        self.name = name
        self.intent = intent
        self.entities = entities
        self.output_channel = output_channel #
        self.reminder_manager = reminder_manager
        self.kill_on_user_message = kill_on_user_message
        loop = asyncio.get_event_loop()
        self.task = loop.create_task(self._schedule())
        reminder_manager.update_reminder(self)

    def to_dict(self):

        return dict(sender_id = self.sender_id, name = self.name, intent = self.intent, entities = self.entities, output_channel = self.output_channel, schedule = self.schedule.isoformat(), kill_on_user_message = self.kill_on_user_message)
    
    def update(self, **kwargs):
        schedule = kwargs.get('schedule',None)
        self.task.cancel()
        self.__dict__.update(kwargs)
        if isinstance(schedule,float):
            self.schedule = datetime.datetime.fromtimestamp(schedule)
        elif isinstance(schedule,str):
            self.schedule = datetime.datetime.fromisoformat(schedule)
        else:
            self.schedule = schedule
        loop = asyncio.get_event_loop()
        self.task = loop.create_task(self._schedule())
        self.reminder_manager.update_reminder(self)

    def cancel(self):
        self.reminder_manager.delete_reminder(self)
        return self.task.cancel()
    
    async def _schedule(self):
        # Calculate the time until the next minute
        now = datetime.datetime.now()
        logger.debug(f"Current time {now.isoformat()}")
        duration = self.schedule - now
        total_wait_seconds = duration.total_seconds()
        logger.debug(f"Scheduled {self.name} with intent {self.intent} and entities {self.entities} on {self.schedule} with {total_wait_seconds} wait time")
        await asyncio.sleep(total_wait_seconds)
        logger.debug(f"Resume {self.name}")

        data = dict(name = self.intent, entities = self.entities)

        if RASA_URL:
            try:
                async with aiohttp.ClientSession() as session:
                    execute_reminder = True                    
                    if self.kill_on_user_message:
                        logger.debug("Checking user message")
                        execute_reminder = False
                        async with session.get(f"{RASA_URL}/conversations/{self.sender_id}/tracker?include_events=APPLIED") as response:

                            if response.status == 200:
                                tracker = await response.json()
                                logger.debug(f"Tracker data: {tracker}")
                                for e in reversed(tracker['events']):
                                    if e['event'] == 'reminder' and e['name'] == self.name:
                                        execute_reminder = True
                                        break

                                    if e['event'] == 'user' and 'text' in e:
                                        break


                            else:
                                logger.error(f"Failed to retrieve tracker for conversation: {response.status}",extra=dict(result = await response.text()))

                    if execute_reminder:
                        async with session.post(f"{RASA_URL}/conversations/{self.sender_id}/trigger_intent?output_channel={self.output_channel}",json=data) as response:

                            if response.status == 200:
                                logger.debug(f"Reminder sent to Rasa successfully: {data}") # DO NOT READ RESULT BODY, WE DO NOT NEED IT
                            else:
                                logger.error(f"Failed to send reminder to Rasa: {response.status}",extra=dict(result = await response.text()))
                    else:
                        logger.debug(f"Reminder {self.name} cancelled by user message")

            except Exception as e:
                logger.error(f"Exception occurred while sending reminder to Rasa: {str(e)}")
        else:
            logger.warn(f"RASA_URL environment not set, skipping API call for conversation {self.sender_id} with body: {data}")
        logger.debug("Reminder finished")
        self.reminder_manager.delete_reminder(self)

class ReminderManager(ABC):
    reminder_manager = None

    def create_reminder(self,sender_id, name, **kwargs):
        logger.info("Create reminder from ReminderManager")
        reminder = Reminder(reminder_manager = self,sender_id=sender_id,name=name, **kwargs)
        return reminder

    def to_dict(self):
        reminders = self.get_reminders()
        now = datetime.datetime.now()
        result = {}
        reminderlist = []
        for sender_id,name in reminders:
            reminder = self.get_reminder(sender_id,name)
            rdict = reminder.to_dict()
            reminderlist.append(rdict)
        result['reminders'] = reminderlist
        return result


    @abstractmethod
    def get_reminders(self):
        pass

    @abstractmethod
    def get_reminder(self,sender_id, name):
        pass

    @abstractmethod
    def update_reminder(self,reminder):
        pass

    @abstractmethod
    def delete_reminder(self,reminder):
        pass

    @classmethod
    def get_reminder_manager(cls):
        pass
        if not cls.reminder_manager:
            if not REDIS_URL:
                cls.reminder_manager = SimpleReminderManager()
            else:
                cls.reminder_manager = RedisReminderManager()
        return cls.reminder_manager
        
class SimpleReminderManager(ReminderManager):
    def __init__(self):
        self.reminders = {}

    def get_reminder(self,sender_id,name):
        reminder = self.reminders.get((sender_id,name))
        return reminder

    def get_reminders(self):
        return self.reminders.values()

    def update_reminder(self,reminder):
        self.reminders[(reminder.sender_id, reminder.name)] = reminder
        
    def delete_reminder(self,reminder):
        return self.reminders.pop((reminder.sender_id, reminder.name), None)


class RedisReminderManager(ReminderManager):
    def __init__(self):
        self.reminders = {}
        self.reminderdata = redis.Redis.from_url(url = REDIS_URL, decode_responses=True)
        reminderlist = []
        p_reminderlist = self.reminderdata.scan(match=f"{REDIS_PREFIX}*")
        reminderlist.extend(p_reminderlist[1])
        while p_reminderlist[0]>0:
            p_reminderlist = self.reminderdata.scan(p_reminderlist[0],match=f"{REDIS_PREFIX}*")
            reminderlist.extend(p_reminderlist[1])
        for r in reminderlist:
            data = json.loads(self.reminderdata.get(r))
            sender_id = data.pop('sender_id')
            name = data.pop('name')
            self.create_reminder(sender_id,name,**data)


    def get_reminder(self,sender_id,name):
        reminder = self.reminders.get((sender_id,name))
        return reminder

    def get_reminders(self):
        return self.reminders.values()

    def update_reminder(self,reminder):
        self.reminders[(reminder.sender_id, reminder.name)] = reminder
        self.reminderdata.set(f"{REDIS_PREFIX}{reminder.sender_id}_{reminder.name}", json.dumps(reminder.to_dict()))
       
    def delete_reminder(self,reminder):
        self.reminderdata.delete(f"{REDIS_PREFIX}{reminder.sender_id}_{reminder.name}")
        return self.reminders.pop((reminder.sender_id, reminder.name), None)

# class RedisReminderManager(ReminderManager):

#     def __init__(self):
#         self.sessions = {} #prevent new instances, always check redis for existence
#         self.sessiondata = Redis.from_url(url = REDIS_URL, decode_responses=True)

#     def get_sessions(self):
#         sids = []
#         p_sids = self.sessiondata.scan()
#         sids.extend(p_sids[1])
#         while p_sids[0]>0:
#             p_sids = self.sessiondata.scan(p_sids[0])
#             sids.extend(p_sids[1])
#         if RedisSessionManager.CALLBACK_QUEUE in sids:
#             sids.remove(RedisSessionManager.CALLBACK_QUEUE)
#         return sids

#     def get_session(self,sender_id):
#         sessionstr = self.sessiondata.get(sender_id)
#         session = self.sessions.get(sender_id) if sessionstr else None
#         if sessionstr and not session: #only when it is available in redis, no autocreate
#             logger.info("restoring session from redis")
#             data = json.loads(sessionstr)
#             guid = data.pop('guid')
#             data.pop('sender_id')
#             # if 'log' in data:
#             #     data.pop('log')
#             session = self.create_session(guid,sender_id,**data) 
#             self.sessions[sender_id] = session
#         else:
#             logger.info("reusing session object from memory")

#         return session

#     def update_session(self,session):
#         self.sessions[session.sender_id] = session
#         self.sessiondata.set(session.sender_id, json.dumps(session.to_dict()))

    
#     def delete_session(self,session):
#         self.sessiondata.delete(session.sender_id)
#         self.sessions.pop(session.sender_id, None)



apiv1 = Blueprint("api_v1",version=1, version_prefix="/api/v")

@apiv1.get("/reminders")
async def reminders(request):
    logger.info(f"receiving request /reminders")
    rm = ReminderManager.get_reminder_manager()
    reminders = rm.get_reminders()
    return response.json([reminder.to_dict() for reminder in reminders])   

@apiv1.put("/reminders/<sender_id>/<name>")
async def update_reminder(request,sender_id,name):
    logger.info(f"receiving request {sender_id} {name} with data {request.json}")
    data = request.json
    rm = ReminderManager.get_reminder_manager()
    r = rm.get_reminder(sender_id,name)
    if not r:
        r = rm.create_reminder(sender_id,name,**data)
    else:
        r.update(**data)
    return response.json({"status": "ok"})

@apiv1.delete("/reminders/<sender_id>/<name>")
async def delete_reminder(request,sender_id,name):
    data = request.json
    rm = ReminderManager.get_reminder_manager()
    r = rm.get_reminder(sender_id,name)
    if r:
        r.cancel()
    return response.json({"status": "ok"})

