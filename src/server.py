#!/usr/local/bin/python
import asyncio
import logging
import aiohttp
import itertools
import copy
from aiohttp import web
import os
from datetime import timezone
import datetime
from apiv1 import apiv1

import requests
from elasticapm.contrib.sanic import ElasticAPM
from sanic import Blueprint, Sanic, response, exceptions
from sanic.request import Request
from sanic.response import HTTPResponse, json, text
import jwt



sentry_dsn = os.getenv("ROUTER_SENTRY_DSN", None)
if sentry_dsn:
    import sentry_sdk
    from sentry_sdk.integrations.sanic import SanicIntegration
    from sentry_sdk.integrations.redis import RedisIntegration

    sentry_sdk.init(dsn=sentry_dsn, integrations=[SanicIntegration(), RedisIntegration()])


app = Sanic(name="router")
app.config.SECRET = "KEEP_IT_SECRET_KEEP_IT_SAFE"
app.blueprint(apiv1)

if os.getenv("ELASTIC_APM_SERVER_URL"):
    environment = os.getenv("ENVIRONMENT")
    app.config.update({
        "ELASTIC_APM_SERVER_URL": os.getenv("ELASTIC_APM_SERVER_URL"),
        "ELASTIC_APM_SECRET_TOKEN": os.getenv("ELASTIC_APM_SECRET_TOKEN"),
    })
    apm = ElasticAPM(app=app, config={"SERVICE_NAME": f"Environment {environment}"})


@app.get("/login")
async def do_login(request):

    token = {'sub':'test@test.nl','aud':'statistics','exp':datetime.datetime.now(tz=timezone.utc) + datetime.timedelta(seconds=3600)}
    if 'sub' in request.args:
        token['sub'] = request.args['sub'][0]
    # if 'uname' in request.args:
    #     token['uname'] = request.args['uname'][0]
    if 'org' in request.args:
        token['org'] = request.args['org'][0]
    enc = jwt.encode(token, request.app.config.SECRET)
    token['token'] = enc
    token.pop('exp')
    return json(token)

@app.route("/", methods=["GET"])
async def health(_: Request) -> HTTPResponse:
    return response.json({"status": "ok","service":"rasa-scheduler"})

if __name__ == "__main__":
    app.run(host="0.0.0.0")
